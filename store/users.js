import axios from 'axios';
export const state = () => ({
    users: [],
    infoAboutUser: [],
})
export const mutations = {
    setPosts(state, users) {
        state.users = users
    },
    INFO_ABOUT_USER(state, info) {
      state.infoAboutUser = info
    }
}
export const actions = {

    async FETCH({commit}) {
      // Принимает данные всех пользователей и сортирует их
      // const test = axios.get('http://www.filltext.com/?rows=100&id={number|1000}&firstName={firstName}&lastName={lastName}&email={email}&phone={phone|(xxx)xxx-xx-xx}&adress={addressObject}&description={lorem|32}');
      const res = await fetch("http://www.filltext.com/?rows=100&id={number|1000}&firstName={firstName}&lastName={lastName}&email={email}&phone={phone|(xxx)xxx-xx-xx}&adress={addressObject}&description={lorem|32}");
      const posts = await res.json();
      const sortedArr = [];
      // Код проверки на совпадения id
        const cloningArray = JSON.parse(JSON.stringify(posts));
        const uniqueUsers = [];
        const arrayId = cloningArray.map(elem => elem.id);
        const uniqueId = [...new Set(arrayId)];
        const sortedArray = cloningArray.map((elem, i) => {
            if(elem.id == uniqueId[i]) return elem;
            else cloningArray.splice(i, 1);
        });
      sortedArray.forEach((elem) => elem && uniqueUsers.push(elem));
      // Код проверки на совпадения id
      let size = 20;
      for (let i = 0; i < Math.ceil(uniqueUsers.length/size); i++) {
          sortedArr[i] = uniqueUsers.slice((i*size), (i*size) + size);
      }
      commit('setPosts', sortedArr);
    },
}
export const getters = {
  users: p => p.users
}
